lily-marktest
=============

This package provides testing for Lily code in markdown files. This package
contains the testing library as well as a wrapper script to invoke the library
over a single markdown file.

The marktest library reads code within fenced code blocks. To avoid running code
of other languages, only blocks that specify 'lily' as a language are run. Also,
the blocks must not have any preceding space.

This code block will be ignored:

```
0 / 0
```

but this one will be executed:

```lily
var elements = ["1", "2", "3"]

elements.map(String.parse_i)
```

Each code block is executed in a different interpreter. This has the benefit of
allowing name reuse as well as avoiding side-effects from previous executions.

```lily
var elements = 1
```

However, it has the drawback that classes and enums need to be declared in each
code block they are executed within.

Testing is done by writing a print command with a comment and the expected
value.

```lily
var numbers = [1, 2, 3].map(|x| x * x )

print(numbers) # [1, 4, 9]
```

However, running tests will not actually invoke print. The print command is
replaced with a different one that logs the value passed. The replacement is
done through text replacement since predefined functions cannot be redefined.

Each print command maps one-to-one with a line of output. Tests using loops
should use a List like so:

```lily
var numbers: List[Integer] = []

for i in 0...50 by 10: {
    numbers.push(i)
}

print(numbers) # [0, 10, 20, 30, 40, 50]
```

In some cases like the interpolation of a class, the actual address of the class
cannot be known ahead of time. In those cases, use `---` which acts like `*` in
wildcard matching.

```
class Point(x: Integer, y: Integer)
{
    public var @x = x
    public var @y = y
}

var p = Point(1, 2)

print(p) # <Point at 0x--->
```

This can be installed using Lily's `garden` via:

`garden install FascinatedBox/lily-marktest`
