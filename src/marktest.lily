import (Interpreter) spawni

class MarkTestResults(pass_count: Integer,
                      fail_count: Integer)
{
    public var @pass_count = pass_count
    public var @fail_count = fail_count
    public var @total = pass_count + fail_count
}

class MarkTestGroup(targets: List[Tuple[Integer, List[String]]],
                    path: String,
                    on_error: Function(String))
{
    private var @targets = targets
    private var @path = path
    private var @on_error = on_error

    private var @pass_count = 0
    private var @fail_count = 0

    private define run_setup_code(interp: Interpreter): Boolean
    {
        var ok = interp.parse_string("[testcode]",
        """
            var testcode__expect: List[String] = []
            var testcode__given: List[String] = []

            class TestError(message: String) < Exception(message) {  }

            define testcode__print[A](value: A)
            {
                testcode__given.push(value ++ "")
            }

            define testcode__equal: Boolean
            {
                if testcode__expect.size() != testcode__given.size(): {
                    return false
                }

                var ok = true

                for i in 0...testcode__expect.size() - 1: {
                    var expect = testcode__expect[i]
                    var given = testcode__given[i]

                    if expect == given: {
                        continue
                    }

                    var split_expect = expect.split("---")

                    # Allow one --- to act as a wildcard (shouldn't need
                    # more than that).

                    if split_expect.size() != 2: {
                        ok = false
                        break
                    }

                    if given.starts_with(split_expect[0]) &&
                       given.ends_with(split_expect[1]): {
                        continue
                    }

                    ok = false
                    break
                }

                return ok
            }

            define testcode__verify
            {
                if testcode__expect == testcode__given ||
                   testcode__equal(): {
                    return
                }

                var e = testcode__expect
                var g = testcode__given

                var expect = e.map(|m| "        " ++ m )
                              .join("\\n")

                var given = g.map(|m| "        " ++ m )
                             .join("\\n")

                var f = \"""\\
                    Mismatch in print values.\\n    \\
                        Expected:\\n\\
                            {0}\\n    \\
                        Received:\\n\\
                            {1}\\
                \""".format(expect, given)

                raise TestError(f)
            }
        """)

        return ok
    }

    private define run_header_code(interp: Interpreter,
                                   ok: Boolean,
                                   header: String): Boolean
    {
        if ok == false: {
            return ok
        }

        ok = interp.parse_string("[testcode]",
        """
            testcode__expect = {0}
        """.format(header))

        return ok
    }

    private define run_test_code(interp: Interpreter,
                                 ok: Boolean,
                                 offset: Integer,
                                 code_to_run: String): Boolean
    {
        if ok == false: {
            return ok
        }

        ok = interp.parse_string("[marktest]", code_to_run)

        return ok
    }

    private define header_for_lines(lines: List[String]): String
    {
        var expect_lines: List[String] = []

        for i in 0...lines.size() - 1: {
            var l = lines[i]

            if l.find("print(").is_none(): {
                continue
            }

            l = l.lstrip(" \t")

            if l.starts_with("print(") == false: {
                continue
            }

            var expect = ""

            try: {
                # Extract the expected value. If none is given, the test will
                # fail from a mismatch later on.
                expect = l.split("#")[1]
                          .trim()
            except IndexError:
            }

            expect_lines.push(expect)

            # Call the testing print instead of the regular one. It's defined
            # in the setup code.
            lines[i] = "testcode__" ++ l
        }

        # Interpolation auto-escapes any tricky characters, notably newlines.
        return expect_lines ++ ""
    }

    private define fixup_error(interp: Interpreter, offset: Integer): String
    {
        var error_lines = interp.error().split("\n")
        var j = 0

        for i in 0...error_lines.size() - 1: {
            if error_lines[i].starts_with("Traceback:"): {
                j = i + 1
                break
            }
        }

        # If the verify fails, this weird line is at the top of the trace. Get
        # rid of it since the error message is sufficiently useful.
        if error_lines[j].ends_with("testcode__verify"): {
            error_lines.delete_at(j)
        }

        for i in j...error_lines.size() - 1: {
            var l = error_lines[i]

            if l.starts_with("    from [marktest]") == false: {
                continue
            }

            var split_line = l.split(":")
            var line = offset + split_line[1].parse_i().unwrap()

            split_line[0] = "    from " ++ @path
            split_line[1] = line.to_s()

            error_lines[i] = split_line.join(":")
        }

        return error_lines.join("\n")
    }

    public define run: MarkTestResults
    {
        var local_targets = @targets

        @targets = []

        for i in 0...local_targets.size() - 1: {
            var t = local_targets[i]
            var offset = t[0]
            var lines = t[1]

            lines.push("\ntestcode__verify()")

            var header = header_for_lines(lines)
            var code_to_run = lines.join("")
            var interp = Interpreter()

            # This is likely to always be true since there shouldn't be a reason
            # for the setup code to fail (unless it was updated incorrectly).
            var ok = run_setup_code(interp)

            ok = run_header_code(interp, ok, header)
            ok = run_test_code(interp, ok, offset, code_to_run)

            if ok == false: {
                var fixed_error = fixup_error(interp, offset)

                @fail_count += 1
                @on_error(fixed_error)
            else:
                @pass_count += 1
            }
        }

        return MarkTestResults(@pass_count, @fail_count)
    }
}

class MarkTest
{
    public var @on_error = (|x: String| unit )

    private define ranges_for_lines(lines: List[String]): List[Integer]
    {
        var begin_block = "```lily\n"
        var end_block = "```\n"
        var expect = begin_block
        var result: List[Integer] = []

        for i in 0...lines.size() - 1: {
            var l = lines[i]

            if l == expect: {
                if expect == begin_block: {
                    expect = end_block
                    result.push(i + 1)
                else:
                    expect = begin_block
                    result.push(i)
                }
            }
        }

        return result
    }

    private define lines_to_targets(lines: List[String])
        : List[Tuple[Integer, List[String]]]
    {
        var ranges = ranges_for_lines(lines)
        var result: List[Tuple[Integer, List[String]]] = []

        for i in 0...ranges.size() - 1 by 2: {
            var start = ranges[i]
            var end = ranges[i + 1]
            var slice = lines.slice(start, end)

            result.push(<[start, slice]>)
        }

        return result
    }

    public define read_file(path: String): Result[String, MarkTestGroup]
    {
        var lines: List[String] = []

        try: {
            var f = File.open(path, "r")
            f.each_line(|l| l.encode().unwrap() |> lines.push )
            f.close()
        except IOError as e:
            return Failure("Cannot open '{0}'.".format(path))
        }

        var targets = lines_to_targets(lines)
        var group = MarkTestGroup(targets, path, @on_error)

        return Success(group)
    }
}
